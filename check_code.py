import json
import re
from unittest import result
import requests
import random
import string
from products import products
from products_keyword import products_keyword
from violation_condition import VIOLATIONS_CONDITION

url = "http://35.209.10.239:2226/webhooks/rest/webhook"

KEYWORDS = {
    "KID_1": [
        "free sample",
        "sample",
        "trial",
        "free trial",
        "FREE 300g* with purchase",
        "FREE 600g* with purchase",
        "FREE 900g* with purchase",
    ],
    "KID_2": [
        "discount",
        "coupons",
        "premiums",
        "rewards",
        "point accumulation for rewards",
        "promotion","Fantastic deal",
        "deals", "price off", "promo", "black friday","vouchers", "Price-off","12% off"
    ],
    "KID_3": [
        "free gift",
        "give gift",
        "gift",
        "incentives",
        "Get exclusive", "vouchers", "prizes", "egift", "Gift card"
    ],
    "KID_4": [
        "join us",
        "subcribe",
        "register",
        "join club",
        "visit us",
        "scan qr to join group",
        "sign up", "join", "follow this link", "link", "click here", "get started now", " learn more"
    ],
    "KID_5": [
        "2'FL-HMO", "prebiotic", "support digestive", "iMMUNIFY ingredients", "iMMUNIFY",
        "immune health", "colostrum", "pronutura", "oligosaccharide", "dHA", "a&A", "aA", "aRA",
        "support immunity", "prebiotics", "fOS", "vitamin A", "vitamin B16", "vitamin B12",
        "vitamin C", "vitamin D", "copper", "folic Adic", "iron", "selenium", "zinc", "improve bone strength",
        "strong bone", "calcium", "2'-FL*", "Zinc", "folate", "minerals", "vitamins", "Omega-3 fatty acids",
        "Omega-6", "essential Vitamins","imuniti", "probiotik", "IQ", "EQ", "anti-colic",
        "GOSl Antibioticsl Sucrose", "nucleotides", "aid brain & visual development", "natural vitamin",
        "vitamin k2", "5mo", "probiotics", "hypollergenic", "sensitive tummies", "iron", "lactoferrin",
        "immune support", "mfgm", "omega", "brain building", "gut health", "prebiotic"
    ],
    "KID_6": [
        "Sponsoring materials", "Fellowships", "Professional conferenceser",
        "Research", "Programs", "Webinar", "live", "Q&A WITH SHAE", "livestream "
    ],
    "KID_7": [
        "Healthcare staff", "Health professionals", "Medical professionals", "Medical personnel",
        "Health worker", "Staff in the healthcare industry"
    ],
    "KID_8": [
        "Provide services",
        "Give equipment", "Equipment and services distribution", "Donate equipment",
        "Donate services", "Distribute equipment", "Distribute services"
    ],
    "KID_9": [
        "Events", "Contests", "Campaigns", "Competitions"
    ],
    "KID_10": [
        "nipple", "Bottle", "bottles", "teats", "teat",
    ],
    "KID_11": [
        "Nestle Baby and me", "Nestle Baby & me","Nestlé Baby & Me","Nestlé Baby and Me","Karikhome",
        "Nestle MOM", "Nestle","Lactogrow","Lactogen","PreNAN","NIDO","CERELAC","GERBER","SMA","NAN",
        "FRISO MUM","Peak","Friso","Aptamil","Cow & Gate","Dumex Mamil","Enfamil","Nutramigen","Enfagrow",
        "PurAmino"
    ],
    "KID_12": [
        "breast-like", "natural bottle", "natural feed", "natural nipple"
    ]
}


from threading import Thread
import threading

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def callAPINLP(message):
    payload = json.dumps({
        "sender": id_generator(),
        "message": message
    })
    headers = {
        'Content-Type':'application/json'
    } 
    response = requests.request("POST", url, headers=headers, data=payload, timeout=20)
    rs = response.json()
    check_code = 'ID_other'
    if(len(rs) == 0):
      payload = json.dumps({
          "sender": 'a',
          "message": message
      })
      headers = {
          'Content-Type':'application/json'
      } 
      response = requests.request("POST", url, headers=headers, data=payload, timeout=20)
      rs = response.json()
    if(len(rs) > 0):
        check_code = rs[0]['text']
    
    return [check_code]


import time
def is_contain(keyword, line_text):
    regex_pattern = (
        rf'''.*\b{
        (
            "[^0-9a-zA-Z]{0,}".join(keyword.split(' '))
        )
        }\b.*'''
    )
    return bool(re.search(regex_pattern, line_text))
def check_ID_KiD(line_text, result, i):
# def check_ID_KiD(line_text):
    line_text = line_text.lower()
    list_product_categories_by_keywords = []
    list_kid = []
    list_id_code = []
    # checking keywords
    for kid, list_keywords in KEYWORDS.items():
        is_error_keywords = any(
            is_contain(word, line_text)
            for word in [
                keyword.lower() for keyword in list_keywords
            ]
        )
        if is_error_keywords:
            list_kid.append(kid)
    # check NLP
    if(len(line_text)>30):
        check_code = callAPINLP(line_text)
        list_id_code.append(check_code[0])
    # check_code = callAPINLP(line_text)
    # list_id_code.append(check_code[0])
    # checking product type keywords
    for product_category, all_list_keyword in products_keyword.items():
        for list_keyword in all_list_keyword:
            list_keyword = [item.lower() for item in list_keyword]
            if all(
                    is_contain(keyword, line_text)
                    for keyword in list_keyword
            ):
                list_product_categories_by_keywords.append(product_category)
                break
    if(not list_id_code):
        list_id_code=['ID_other']
    result[i] = [list_kid, list_product_categories_by_keywords, list_id_code]
import numpy as np
# def check_violation(captions, list_product_type, feeding=False, sub_domain=False, fb_insta=False):
def check_violation(captions, list_product_type, feeding=False, sub_domain=False, fb_insta=False):
    import re

    violations_codes = []

    list_kid = []
    list_id_code = []
    list_product_categories_by_keywords = []
    list_product_categories_by_image = []

    list_line_text = re.split("[?!.;\n]", captions)
    threads = [None] * len(list_line_text)
    result = [None] * len(list_line_text)
    # list_kid = [None] * len(list_line_text)
    # list_product_categories_by_keywords = [None] * len(list_line_text)
    for i in range (len(threads)):
        threads[i] = threading.Thread(target= check_ID_KiD, args=(list_line_text[i], result, i))
        threads[i].start()
    for i in range (len(threads)):
        threads[i].join()
    # print(result)
    for item in result:
        list_kid += item[0]
        list_product_categories_by_keywords +=item[1]
        list_id_code += item[2]

    # print(
    #     f"\n listKid: {list_kid} ,\n keyword: {list_product_categories_by_keywords},\n nlp: {list_id_code}, "
    # )

    # checking product type image
    for key, list_product_validate in products.items():
        if set(list_product_type) & set(list_product_validate):
            list_product_categories_by_image.append(key)
    if feeding:
        list_product_categories_by_image.append("Feeding")

    list_kid = list(set(list_kid))
    list_product_categories_by_image = list(set(list_product_categories_by_image))
    list_product_categories_by_keywords = list(set(list_product_categories_by_keywords))
    list_id_code = list(set(list_id_code))

    print(
        f"\n listKid: {list_kid} ,\n image: {list_product_categories_by_image},\n keyword: {list_product_categories_by_keywords},\n nlp: {list_id_code}, "
    )

    for code, conditions in VIOLATIONS_CONDITION.items():

        matched = False
        has_included_not_nlp_conditions = any(condition.get('nlp', {}).get('not_included') for condition in conditions)
        for condition in conditions:
            # checking condition
            image = condition.get('image', [])
            nlp = condition.get('nlp', {})
            product_type_keyword = condition.get('product_type_keyword', [])
            keyword = condition.get('keyword', [])
            sub_domain_condition = condition.get('sub_domain', False)
            # fb_insta_condition = condition.get('fb_insta', False)

            checking_image = False
            checking_nlp = False
            checking_product_type_keyword = False
            checking_keyword = False
            checking_sub_domain = False
            # checking_fb_insta = False

            if image:
                if list_product_categories_by_image:
                    checking_image = set(image) & set(list_product_categories_by_image)
            else:
                checking_image = True

            if keyword:
                if list_kid:
                    checking_keyword = len(set(list_kid) & set(keyword)) == len(keyword)
            else:
                checking_keyword = True

            if product_type_keyword:
                if list_product_categories_by_keywords:
                    if isinstance(product_type_keyword[0], list):
                        checking_product_type_keyword = (
                                set(list_product_categories_by_keywords) & set(product_type_keyword[0])
                                and
                                set(list_product_categories_by_keywords) & set(product_type_keyword[1])
                        )
                    else:
                        checking_product_type_keyword = set(list_product_categories_by_keywords) & set(
                            product_type_keyword)
            else:
                checking_product_type_keyword = True
            if sub_domain_condition:
                checking_sub_domain = sub_domain
            else:
                checking_sub_domain = True

            # if fb_insta_condition:
            #     checking_fb_insta = fb_insta
            # else:
            #     checking_fb_insta = True

            if nlp and (nlp['must_included'] or nlp['not_included']):
                if list_id_code:
                    # Captions is a violation if contain all the code in MUST_INCLUDED list
                    # and not contain code in NOT_INCLUDED list
                    if nlp['must_included']:
                        checking_nlp = len(set(list_id_code) & set(nlp['must_included'])) == len(nlp['must_included'])
                    else:
                        checking_nlp = True
                    if nlp['not_included']:
                        checking_nlp &= not bool(set(list_id_code) & set(nlp['not_included']))
            else:
                checking_nlp = True

            other_check_matched = ( 
                checking_image and
                checking_keyword and
                checking_product_type_keyword and
                checking_sub_domain
                # and checking_fb_insta
            )

            condition_matched = other_check_matched and checking_nlp
            included_not_nlp = condition.get('nlp', {}).get('not_included', [])

            if not has_included_not_nlp_conditions:
                if condition_matched:
                    matched = True
                    break
            else:
                if not condition_matched:
                    if included_not_nlp and other_check_matched:
                        matched = False
                        break
                else:
                    matched = True

        if matched:
            violations_codes.append(code)

    print(violations_codes)
    return violations_codes


input_string = """
We've expanded into cow milk! Karihome Cow Milk Growing-Up Formula contains 2'-FL MultiShield Protect to fortify your child's immune system and digestive health besides promoting physical and mental development.
"""
time_start = time.time()
check_violation(
    input_string,
    ['Bellamy_12'
    ]
)
time_excu = time.time()-time_start
print(time_excu)
# list1, list2, list3 = check_ID_KiD(input_string)
# print(list1, list2, list3)
