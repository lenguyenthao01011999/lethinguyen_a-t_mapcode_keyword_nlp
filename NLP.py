from csv import list_dialects
import requests
import json
# import re

url = "http://13.228.232.118:8001/webhooks/rest/webhook"
violations = {
    "5.1": [["ID_1"]],
    "5.3": [["ID_2"]],
    "WHA58.32-Info.Edu_contam": [["ID_3","ID_4"]],
    "WHA58.32-Info.HW_contam": [["ID_4"]],
    "WHA58.32-Info.HW_claims": [["ID_5"]],
    "WHA58.32-Info.Edu_contam": [["ID_3","ID_5"], ['ID_3']],
    "4": [["ID_3"]],
    "4.3": [["ID_3"]],
    "7.5; WHA49.15; WHA58.32": [["ID_15","ID_3"], ["ID_3"]],
    "WHO.Rec5-Promotion": [["ID_2"]],
    "WHO.Rec6-Sample": [["ID_21"]],
    "WHO.Rec6- COI": [["ID_2"], ["ID_22"], ["ID_23"], ["ID_24"]],
    "WHA63.23-CF_claims": [["ID_5"]],
    "4.2": [
        ["ID_3", "ID_6"],
        ["ID_3","ID_16"],
        ["ID_3","ID_8"],
        ["ID_3","ID_9"],
        ["ID_3","ID_10"],
        ["ID_3","ID_11"],
        ["ID_3","ID_12"],
    ],
    "7.2": [
        ["ID_13"],
        ["ID_14"],
        ["ID_16"],
        ["ID_8"],
        ["ID_9"],
        ["ID_12"],
    ],
    "WHO.Rec 4": [
        ["ID_11"],
        ["ID_7"],
        ["ID_17"],
        ["ID_18"],
        ["ID_19"],
        ["ID_20"],
    ],
}

keywords = {
    "KID_1":[
        "free sample",
        "sample",
        "trial",
        "free trial"
    ],
    "KID_2":[
        "discount",
        "coupons",
        "premiums",
        "rewards",
        "point accumulation for rewards",
        "tie-in / bundled sales",
        "promotion"
    ],
    "KID_3":[
        "free gift",
        "give gift",
        "gift",
        "incentives"
    ],
    "KID_4":[
        "join us",
        "subcribe",
        "register",
        "scan qr to join group"
    ],
    "KID_5":[
        "2'FL-HMO","prebiotic","support digestive","iMMUNIFY ingredients","iMMUNIFY",
        "immune health","colostrum","pronutura","oligosaccharide","dHA","a&A","aA","aRA",
        "support immunity","prebiotics","fOS","vitamin A","vitamin B16","vitamin B12",
        "vitamin C","vitamin D","copper","folic Adic","iron","selenium","zinc","improve bone strength",
        "strong bone","calcium"
    ],
    "KID_6":[
        "BMS","Pregnancy products","Feeding bottles and teats"
    ],
    "KID_7":[
        "Sponsoring materials","Fellowships","Professional conferenceser",
        "Research","Programs","Webinar"
    ],
    "KID_8":[
        "Healthcare staff","Health professionals","Medical professionals","Medical personnel",
        "Health worker","Staff in the healthcare industry"
    ],
    "KID_9":[
        "Provide services",
        "Give equipment","Equipment and services distribution","Donate equipment",
        "Donate services","Distribute equipment","Distribute services"
    ],
    "KID_10":[
        "Events","Contests","Campaigns","Competitions"
    ],
}

violations_keyword = {
    "5.2":[["KID_1"]],
    "5.3":[["KID_2"]],
    "5.4":[["KID_3"]],
    "5.5":[["KID_4"]],
    "WHA58.32-Info.HW_claims":[["KID_5"]],
    "4.3":[["KID_6"]],
    "7.5; WHA49.15; WHA58.32":[["KID_7","KID_8"]],
    "WHO.Rec5-Promotion":[["KID_2"]],
    "WHO.Rec6-Sample":[["KID_1"]],
    "WHO.Rec6- COI":[
        ["KID_8","KID_9"],
        ["KID_8","KID_3"],
        ["KID_8","KID_10"],
        ["KID_8","KID_2"],
        ["KID_8"],
    ],
    "WHA63.23-CF_claims":[["KID_5"]]
}

def get_combinations(input_list):
    from itertools import combinations
    return [
        *[[item] for item in input_list], 
        *list(combinations(input_list,2))
    ]

def get_code_map(list_kid, violations_map, result_map):
    for combination in get_combinations(list_kid):
        code = id2Code(combination, violations_map)
        if code:
            result_map.update(
                **{
                    ";".join(combination): code
                }
            )
    print(result_map)
    return result_map


def id2Code(listId, violations_map):
    listCode=[]
    for code, rules in violations_map.items():
        set_a = set(listId)
        for rule in rules:
            set_b = set(rule)
            if not(set_a - set_b) and not(set_b - set_a):
                listCode.append(code)
    return listCode


def callAPINLP(message):
    payload = json.dumps({
        "sender": "a",
        "message": message
    })
    headers = {
        'Content-Type':'application/json'
    } 
    response = requests.request("POST", url, headers=headers, data=payload)
    return [json.loads(response._content)[0].get("text")]

def check_keyword(captions):
    import re

    result_violations_map: dict = {}
    list_sentences = re.split("[?!.;\n]",captions)
    list_KID = []
    for sentence in list_sentences:
        for kid, list_keywords in keywords.items():
            sentence = sentence.lower()
            isErrorKeywords = any(
                word in sentence 
                for word in [
                    keyword.lower() for keyword in list_keywords
                ]
            )
            if isErrorKeywords:
                list_KID.append(kid)
    if list_KID:
        get_code_map(list_KID, violations_keyword, result_violations_map)
    return result_violations_map

def checkNLP(caption):
    import re

    result_code: dict = {}
    list_line_text = re.split("[?!.;\n]",caption)
    list_id_code = []
    for line_text in list_line_text:
        check_code = callAPINLP(line_text)
        if check_code != ['ID_other']:
            list_id_code.append(check_code[0])
    if list_id_code:
        get_code_map(list_id_code, violations, result_code)
    return result_code

checkNLP(
    """Most new mums want to be able to breastfeed and enjoy the close bond that comes from spending feeding time together. But sometimes mothers can find it difficult, so it’s important that as a new mother you feel supported and know the benefits of breastfeeding – so you can enjoy breastfeeding for longer.
    It’s best to wait until your baby is 3 to 4 weeks old and breastfeeding has been established before introducing a bottle.
    Philips AVENT is @mothercareph’s Brand of the Week! Get your Philips AVENT at both Mothercare boutiques and at www.mothercare.ph for up to 32% off until July 17!
    The big day is coming! Make sure to stock up on your baby essentials when the little one pops out. Be sure to get our bottles, which are not only BPA free, but are also designed to prevent leakage while feeding.
    Your baby just turned 6 months, and the little one is now ready for an important milestone: Solids! Our weaning spoon is up for the job. Its non-slip handle makes it easy to grip, and its soft tip is gentle on Baby’s gums.
    We guarantee that Philip Avent bottles are made of 100% BPA-free material. Why is this important? BPA—or Bisphenol A—is a chemical that can affect a child’s brain and even behavior. That’s why it’s best to steer clear from BPA.
    Our anti-colic baby bottle with AirFree vent locks milk in and keeps air out. This makes for easy upright feeding. It’s important to reduce the air that your baby ingests, as this prevents feeding issues like colic and reflux from happening.
    There’s a pretty good reason why we named this the Natural Baby Bottle. With its unique design, drinking from the bottle feels like latching on to the mother’s breast. It’s bottle feeding made to feel more natural, making it easy to combine it with breastfeeding."""
)
check_keyword(
    """Most new mums want to be able to breastfeed and enjoy the close bond that comes from spending feeding time together. But sometimes mothers can find it difficult, so it’s important that as a new mother you feel supported and know the benefits of breastfeeding – so you can enjoy breastfeeding for longer.
    It’s best to wait until your baby is 3 to 4 weeks old and breastfeeding has been established before introducing a bottle.
    Philips AVENT is @mothercareph’s Brand of the Week! Get your Philips AVENT at both Mothercare boutiques and at www.mothercare.ph for up to 32% off until July 17!
    The big day is coming! Make sure to stock up on your baby essentials when the little one pops out. Be sure to get our bottles, which are not only BPA free, but are also designed to prevent leakage while feeding.
    Your baby just turned 6 months, and the little one is now ready for an important milestone: Solids! Our weaning spoon is up for the job. Its non-slip handle makes it easy to grip, and its soft tip is gentle on Baby’s gums.
    We guarantee that Philip Avent bottles are made of 100% BPA-free material. Why is this important? BPA—or Bisphenol A—is a chemical that can affect a child’s brain and even behavior. That’s why it’s best to steer clear from BPA.
    Our anti-colic baby bottle with AirFree vent locks milk in and keeps air out. This makes for easy upright feeding. It’s important to reduce the air that your baby ingests, as this prevents feeding issues like colic and reflux from happening.
    There’s a pretty good reason why we named this the Natural Baby Bottle."""
)
